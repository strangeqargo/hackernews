### архитектура
- Сервис написан на aiohttp + mysql
- aiohttp висит за nginx
- При старте в отдельном таске в цикле запускается забор данных с hn. 
Частота забора регулируется настройками основного приложения 
- Парсер написан... на list comprehension и bsoup4


### запуск
`docker-compose up`, сервис будет доступен по localhost:8080  

### примеры запросов
```
curl localhost:8080/posts
curl localhost:8080/posts?order=title
curl 'localhost:8080/posts?order=title&dir=BAD' #error
curl 'localhost:8080/posts?order=title&dir=asc'
curl 'localhost:8080/posts?order=title&dir=desc' | jq '.' #check json
curl 'localhost:8080/posts?order=created&dir=desc' 
curl 'localhost:8080/posts?order=url&dir=desc' 
curl 'localhost:8080/posts?order=URL&dir=desc' #error 
curl 'localhost:8080/posts?order=created&dir=desc&limit=10' 
curl 'localhost:8080/posts?order=created&dir=desc&limit=100' #error
curl 'localhost:8080/posts?order=created&dir=desc&limit=-1' #error
curl 'localhost:8080/posts?order=created&dir=desc&limit=5' 
curl 'localhost:8080/posts?order=created&dir=desc&limit=5&offset=2'
```
запросом /update можно принудительно сходить на HN за данными

### тесты

`docker exec -it hackernews bash tests.sh`

тестами покрыты только основные компоненты: парсинг, валидация параметров, 
механизм работы скрапера

### todo:

- нормальный конфиг приложения
- ... 
