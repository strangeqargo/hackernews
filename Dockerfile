FROM python:3.9.1-buster


WORKDIR /hackernews
COPY . .
RUN pip install -r requirements.txt

CMD ["bash"]