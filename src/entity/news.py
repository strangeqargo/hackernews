import datetime
import logging
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.dialects.mysql.dml import Insert

Base = declarative_base()


class NewsItem(Base):
    
    __tablename__ = "news"
    
    ID = Column(Integer, name="id", primary_key=True)
    hn_id = Column(Integer, name="hn_id", unique=True, primary_key=False)
    title = Column(String(255), unique=False, nullable=False, default='', index=True)
    url = Column(String(2083), unique=False, nullable=True, index=True)
    created = Column(DateTime, unique=False, nullable=False, default=datetime.datetime.utcnow, index=True)

    def update(self, iterable=(), **kwargs):
        self.__dict__.update(iterable, **kwargs)

    def __repr__(self):
        return f"Story title='{self.title}', ID={self.ID}"

    @staticmethod
    def bulk_insert(engine, items):
        stmt = Insert(NewsItem)
        stmt = stmt.on_duplicate_key_update(title=stmt.inserted.title,
                                            url=stmt.inserted.url
                                            ).values(
            [[x.ID, x.hn_id, x.title, x.url] for x in items]
        )
    
        engine.execute(stmt)
        

    
def create_structure(engine):
    Base.metadata.create_all(engine)


