"""Hacker News scraper

Usage:
  src/app.py action server
  src/app.py action setup
  
  src/app.py (-h | --help)

Options:
  -h --help     Show this screen.

"""
from docopt import docopt
import logging
from aiohttp import web
import asyncio
import json
from db import get_session
from os import getpid, getenv
from entity.news import create_structure
from handlers.hn_news_handler import handler
from routes.posts import posts
from routes.update import update
logging.basicConfig(filename='/tmp/news.log', level=logging.DEBUG, force=True)


async def start_background_tasks(app):
    app['hnews_handler'] = asyncio.create_task(handler(app))


async def cleanup_background_tasks(app):
    app['hnews_handler'].cancel()
    await app['hnews_handler']


def setup():
    session, engine = get_session(load_config())
    create_structure(engine)


def load_config(config_file='config.json'):
    with open(config_file) as json_data_file:
        config = json.load(json_data_file)
        mysql_override = getenv("HACKERNEWS_MYSQL_HOST")
        if mysql_override:
            config['mysql']['host'] = mysql_override
            
        return config


if __name__ == "__main__":
    
    arguments = docopt(__doc__)
    print(arguments)
    if arguments['setup']:
        setup()
    else:
        app = web.Application()
        config = load_config()
        
        
        logging.debug(f"server mode on, pid {getpid()}")
        # lets inject our scraper into background thread
        app.on_startup.append(start_background_tasks)
        app.on_cleanup.append(cleanup_background_tasks)
        
        # mysql sessions for background tasks and user requests
        app['background_session'], app['background_engine'] = get_session(config)
        app['client_session'], app['client_engine'] = get_session(config, is_async=True)
        app['handler_get_limit'] = 30
        app['items_get_limit_max'] = 30
        app['items_get_limit_default'] = 5
        
        app['fetch_url'] = 'https://news.ycombinator.com/'
        # behavior params
        app['check_period_time'] = 360
        
        app.router.add_get('/posts', posts)
        app.router.add_get('/update', update)
        web.run_app(app, handle_signals=True)

