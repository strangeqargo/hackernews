import logging
from entity.news import NewsItem
from sqlalchemy.ext.asyncio.session import AsyncSession


async def paginator(session: AsyncSession, engine, db_params: dict):
    # sqlalchemy этой версии глючит с limit, пока не используем
    order = {
        "id": NewsItem.ID,
        "title": NewsItem.title,
        "url": NewsItem.url,
        "created": NewsItem.created
    }
    # o = order[db_params["order"]]
    
    # поэтому пишем простой запрос c валидироваными данными
    query = await make_paginator_query(db_params)
    
    out = []
    async with session:
        #async with session.begin():
        result = await session.execute(query)
        #result = await session.execute(select(NewsItem).order_by(NewsItem.ID))
        for row in result:
            row_dict = dict(zip(order.keys(), row))
            row_dict['created'] = row_dict['created'].isoformat()
            out.append(row_dict)
        return out


async def make_paginator_query(db_params):
    o = db_params["order"]
    direction = db_params["dir"]
    limit = db_params["limit"]
    offset = db_params["offset"]

    query = f"""SELECT
    news.id, news.title, news.url, news.created
    FROM news ORDER BY news.{o} {direction}
    LIMIT {limit} OFFSET {offset}"""
    
    return query
