from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.ext.asyncio.session import AsyncSession

import json
import logging
from entity.news import NewsItem


class ServiceDBException(Exception):
    def __init__(self, message):
        self.message = message


def setup_uri(config: dict):
    if config.get('mysql', False):
        mysql_config = config['mysql']
        rdbms = "mysql"
        pip_package = "mysqlconnector"
        return "{}+{}://{}:{}@{}:{}/{}".format(
            rdbms, pip_package, mysql_config['user'], mysql_config['password'],
            mysql_config['host'], mysql_config['port'], mysql_config['database'])
    else:
        raise ServiceDBException("Bad MySQL config")


def get_session(config: dict, is_async: bool = False):
    
    if is_async:
        engine = create_async_engine(setup_uri(config), echo=False, pool_recycle=3600)
        session = AsyncSession(engine)
        return session, engine
    else:
        engine = create_engine(setup_uri(config), pool_recycle=3600)
        session = sessionmaker(bind=engine)
        return session(), engine
    
    # except
    # if engine is None:
    #     raise ServiceDBException("Failed to connect to MySQL")



