from db.pagination import paginator
from routes import ValidatorException
from aiohttp import web


async def validate_params(max_limit, default_limit, query):
    q = query
    
    s = {  # scheme
        "limit": {"min": 1, "max": max_limit},
        "offset": {"min": 0},
        "order": ["id", "title", "url", "created"],
        "dir": ["asc", "desc"]
    }
    
    db_params = {}
    # order
    if q.get('order', 'id') not in s['order']:
        raise ValidatorException(f"Wrong order param. Valid options: {','.join(s['order'])}")
    db_params['order'] = q.get('order', 'id')
    
    # dir
    if q.get('dir', 'asc') not in s['dir']:
        raise ValidatorException(f"Wrong dir param. Valid options: {','.join(s['dir'])}")
    db_params['dir'] = q.get('dir', 'asc')
    
    # limit
    limit = int(q.get("limit", default_limit))
    if limit not in range(1, s['limit']['max'] + 1):
        raise ValidatorException(f"Wrong limit param.")
    db_params["limit"] = limit
    
    # offset
    offset = int(q.get("offset", 0))
    if offset < s['offset']['min']:
        raise ValidatorException(f"Wrong offset param.")
    db_params["offset"] = offset
    
    return db_params
    

async def posts(request):
    try:
        db_params = await validate_params(
            request.app['items_get_limit_max'],
            request.app['items_get_limit_default'],
            request.rel_url.query)
        
    except ValidatorException as e:
        return web.json_response({"status": 400, "error": e.message})
    except ValueError as e:
        return web.json_response({"status": 400, "error": "Bad request"})
    try:
        posts = await paginator(request.app['client_session'],
                                request.app['client_engine'],
                                db_params)
    except Exception as e:
        return web.json_response({"status": 500, "error": "Server error"})
    
    return web.json_response(posts)
