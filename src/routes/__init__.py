import asyncio

from db.pagination import paginator


class ValidatorException(Exception):
    def __init__(self, message):
        self.message = message
        self.status = 400

