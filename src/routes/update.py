from aiohttp import web
from handlers.hn_news_handler import handler


async def update(request):
    result = 'ok'
    try:
        await handler(request.app, True)
    except Exception as e:
        result = 'error'
        pass
    return web.json_response({'result': result})
