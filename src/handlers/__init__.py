import logging
import aiohttp
from aiohttp.client_exceptions import ClientError


async def get_http(url, ct="text"):
    
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                logging.debug(f"{url}: {resp.status}")
                if ct == "json":
                    return await resp.json()
                elif ct == "text":
                    return await resp.text()
    except ClientError as e:
        logging.error("get_http error")
        return None
