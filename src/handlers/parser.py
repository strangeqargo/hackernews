from bs4 import BeautifulSoup
import asyncio
import sys


async def parse_hacker_news(data: str):
    soup = BeautifulSoup(data, 'html.parser')
    
    # parse into a list
    ids = [
        [
            el['id'],
            [
                {'url': link.get('href', ''), 'title': link.get_text()}
                for link in el.find_all('a', class_='storylink')
            ]
        ]
        for el in soup.find_all('tr', class_='athing')
    ]
    
    # flatten
    for i in range(len(ids)):
        # noinspection PyTypeChecker
        ids[i] = {
            'ID': ids[i][0],  # sqlalchemy object id field, if desired
            'url': ids[i][1][0]['url'],
            'title': ids[i][1][0]['title']
        }
    
    return ids


async def parse_file(filename: str):
    with open(filename) as f:
        data = f.read()
        result = parse_hacker_news(data)
        for x in await result:
            print(x)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("""usage: bs4handler.py filename
example: wget https://news.ycombinator.com/ -q -O out.html && bs4handler.py out.html
""")
    loop = asyncio.new_event_loop()
    loop.run_until_complete(parse_file(sys.argv[1]))
