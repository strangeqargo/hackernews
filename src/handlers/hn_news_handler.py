import sys
import logging
import asyncio
from handlers import get_http
from handlers.parser import parse_hacker_news
from entity.news import NewsItem


async def handler(app=None, run_once: bool = False):
    logging.debug("hh_news_handler start")
    news_counter = 0
    while True:
        all_news = []
        try:
            news = await parse_hacker_news(await get_http(app['fetch_url']))
            limit = app.get('handler_get_limit', 30)
            news = news[:limit]
            for story in news:
                story['hn_id'] = story['ID']
                story['ID'] = 0
                item = NewsItem()
                item.update(story)
                all_news.append(item)
            news_counter += len(all_news)
        except asyncio.CancelledError:
            break
        finally:
            pass
        # logging.debug(all_news)
        NewsItem.bulk_insert(app["background_engine"], all_news)
        if run_once:
            break
        await asyncio.sleep(app.get("check_period_time", 360))
    return news_counter


