import pytest
import json
from src.handlers.parser import parse_hacker_news


class TestParser:
    
    def setup(self):
        with open('tests/yc.html', 'r') as fin:
            self.html_source = fin.read()

    @pytest.mark.asyncio
    async def test_good_html_parsing(self):
        parsed = await parse_hacker_news(self.html_source)
        assert len(parsed) == 30
        assert 'ID' in parsed[0]
        assert 'title' in parsed[0]
        assert 'url' in parsed[0]

    @pytest.mark.asyncio
    async def test_bad_html_parsing(self):
        bad_html = self.html_source
        bad_html = bad_html.replace("<", "")
        parsed = await parse_hacker_news(bad_html)
        assert len(parsed) == 0
