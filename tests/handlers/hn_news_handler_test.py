import pytest
from unittest.mock import patch, MagicMock
from src.handlers.hn_news_handler import handler
from src.handlers import get_http


class TestHandler:
    
    def setup(self):
        with open('tests/yc.html', 'r') as fin:
            self.html_source = fin.read()
    
    @pytest.mark.asyncio
    async def fake_http_get(self, url, ct="text"):
        return self.html_source
    
    @pytest.mark.asyncio
    async def test_handler(self, mocker):
        app = {
            "items_get_limit": 30,
            "fetch_url": "http://localhost",
            "background_engine": MagicMock()
        }

        mocker.patch(
            'src.handlers.hn_news_handler.get_http',
            return_value=self.html_source
        )
        
        news_counter = await handler(app, run_once=True)
        assert news_counter == 30

    @pytest.mark.asyncio
    async def test_get_http_error(self):
        result = await get_http("url", "text")
        assert result is None
