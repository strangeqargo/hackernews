import pytest
import json
from src.routes import ValidatorException
from src.routes.posts import validate_params


class TestValidation:
    
    def setup(self):
        pass
    
    @pytest.mark.asyncio
    async def test_limit(self):
        query = {"limit": 100, "dir": "asc", "offset": 20, "order": "title"}
        with pytest.raises(Exception):
            result = await validate_params(10, 5, query)

        query = {"limit": 10, "dir": "asc", "offset": 20, "order": "title"}
        result = await validate_params(10, 5, query)
        assert 'limit' in result

    @pytest.mark.asyncio
    async def test_offset(self):
        query = {"limit": 10, "dir": "asc", "offset": -1, "order": "title"}
        with pytest.raises(Exception):
            result = await validate_params(10, 5, query)

        query = {"limit": 10, "dir": "asc", "offset": "abcd", "order": "title"}
        with pytest.raises(Exception):
            result = await validate_params(10, 5, query)

        query = {"limit": 10, "dir": "asc", "offset": 200, "order": "title"}
        result = await validate_params(10, 5, query)
        assert 'offset' in result
        
    
    @pytest.mark.asyncio
    async def test_order(self):
        query = {"limit": 10, "dir": "asc", "offset": -1, "order": "BAD"}
        with pytest.raises(Exception):
            result = await validate_params(10, 5, query)
        
        order_options = ['title', 'id', 'url', 'created']
        for o in order_options:
            query = {"limit": 10, "dir": "asc", "offset": 1, "order": o}
            result = await validate_params(10, 5, query)
            assert o in result['order']
            
        pass

    @pytest.mark.asyncio
    async def test_direction(self):
        query = {"limit": 10, "dir": "-1", "offset": 0, "order": "url"}
        
        with pytest.raises(Exception):
            result = await validate_params(10, 5, query)
    
        dir_options = ['asc', 'desc']
        for o in dir_options:
            query = {"limit": 10, "dir": o, "offset": 1, "order": "id"}
            result = await validate_params(10, 5, query)
            assert o in result['dir']

        pass

