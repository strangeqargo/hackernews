CREATE DATABASE IF NOT EXISTS `news` ;
CREATE TABLE IF NOT EXISTS `news`.`news`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `hn_id` bigint unique not null,
  `title` varchar(255) NOT NULL,
  `url` varchar(2083) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`(125)),
  KEY `url` (`url`(125)),
  KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;